var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}

var moment = require('moment')

var mongoose = require('mongoose')
var models = require(__base + '/server/mongodb/models.js')
var MONGODB_URI = (process.env.MONGODB_URI || config.db.MONGODB_URI)
mongoose.connect(MONGODB_URI, {useNewUrlParser: true})

const fbService = require(__base + '/service/fb-service.js')

function queryUserExist (idJson, callback) {
  if ('FBID' in idJson) {
    var FBID = idJson.FBID
    models.Members.findOne({
      'FBID': FBID
    }, function (error, queryUserExist) {
      if (error) {
        console.log('error:', error)
        return handleError(error)
      }
      // console.log('queryUserExist: ')
      // console.log(JSON.stringify(queryUserExist, null, 2))
      // error handle when user profile is not in DB
      if (queryUserExist == null) {
        fbService.userInfoRequest(FBID, function (err, userInfo) {
          if (err) {
            console.log(err)
            throw (err)
          }
          // add new elements to json
          userInfo.FBID = FBID
          userInfo.firstName = userInfo.first_name
          delete userInfo.first_name
          userInfo.lastName = userInfo.last_name
          delete userInfo.last_name
          // fix cloud and user's timezone are different 
          userInfo.joinTime = moment().add(userInfo.timezone, 'hours')
          if (userInfo.timezone >= 0) {
            userInfo.joinTime = moment().add(userInfo.timezone, 'hours')
          } else {
            userInfo.joinTime = moment().subtract(Math.abs(userInfo.timezone), 'hours')
          }
          insertUser(userInfo, function (err, insertUserResult) {
            if (err) {
              // error handling code goes here
              console.log('InsertUser ERROR : ', err)
              throw (err)
            }
            callback(null, userInfo)
          })
        })
      } else {
        callback(null, queryUserExist)
      }
    })
  }
  if ('OMOID' in idJson) {
    var OMOID = idJson.OMOID
    models.Members.findOne({
      'OMOID': OMOID
    }, function (error, queryUserExist) {
      if (error) {
        console.log('error:', error)
        return handleError(error)
      }
      // console.log('queryUserExist: ')
      // console.log(JSON.stringify(queryUserExist, null, 2))
      // error handle when user profile is not in DB
      if (queryUserExist == null) {
        var userInfo = {
          'OMOID': OMOID
        }
        // server is earlier 8 hours
        userInfo.joinTime = moment().add(8, 'hours')
        insertUser(userInfo, function (err, insertUserResult) {
          if (err) {
            // error handling code goes here
            console.log('InsertUser ERROR : ', err)
            throw (err)
          }
          callback(null, userInfo)
        })
      } else {
        callback(null, queryUserExist)
      }
    })
  }
  if ('UUID' in idJson) {
    var UUID = idJson.UUID
    models.Members.findOne({
      'UUID': UUID
    }, function (error, queryUserExist) {
      if (error) {
        console.log('error:', error)
        return handleError(error)
      }
      // console.log('queryUserExist: ')
      // console.log(JSON.stringify(queryUserExist, null, 2))
      // error handle when user profile is not in DB
      if (queryUserExist == null) {
        var userInfo = {
          'UUID': UUID
        }
        userInfo.joinTime = moment().add(8, 'hours')
        insertUser(userInfo, function (err, insertUserResult) {
          if (err) {
            // error handling code goes here
            console.log('InsertUser ERROR : ', err)
            throw (err)
          }
          callback(null, userInfo)
        })
      } else {
        callback(null, queryUserExist)
      }
    })
  }
}

function insertUser (userInfo, callback) {
  // console.log('userInfo:')
  // console.log(JSON.stringify(userInfo, null, 2))
  var new_member = new models.Members(userInfo)
  new_member.save(function (error, insertUserResult) {
    if (error) {
      console.log('error:', error)
      throw (error)
    }
    // console.log('insertUserResult_result: ')
    // console.log(JSON.stringify(insertUserResult, null, 2))
    callback(null, insertUserResult)
  })
}

function querySubscribeTopic (FBID, callback) {
  queryUserExist({ 'FBID': FBID }, function (err, queryUserExist) {
    if (err) {
      // error handling code goes here
      console.log('queryUserExist ERROR : ', err)
      throw (err)
    }
    models.Members.findOne({
      'FBID': FBID
    }, 'subscribeTopic', function (error, result) {
      if (error) {
        console.log('error:', error)
        throw (error)
      }
      callback(null, result.subscribeTopic)
    })
  })
}

function queryUserTimezone (FBID, callback) {
  queryUserExist({ 'FBID': FBID }, function (err, queryUserExist) {
    if (err) {
      // error handling code goes here
      console.log('queryUserExist ERROR : ', err)
      throw (err)
    }
    models.Members.findOne({
      'FBID': FBID
    }, 'timezone', function (error, userTimezone) {
      if (error) {
        console.log('error:', error)
        throw (error)
      }
      callback(null, userTimezone)
    })
  })
}

function updateSubscribeTopic (inputJson, callback) {
  queryUserExist({ 'FBID': inputJson.FBID }, function (err, queryUserExist) {
    if (err) {
      // error handling code goes here
      console.log('queryUserExist ERROR : ', err)
      throw (err)
    }
    models.Members.where('FBID', inputJson.FBID).update({
      $set: {
        'subscribeTopic': inputJson.subscribeTopic
      }
    }, function (error, result) {
      // error will be an Error if one occurred during the query
      if (error) {
        console.log('error:', error)
        throw (error)
      }
      callback(null, result)
    })
  })
}

function updatePushTime (inputJson, callback) {
  queryUserExist({ 'FBID': inputJson.FBID }, function (err, queryUserExist) {
    if (err) {
      // error handling code goes here
      console.log('queryUserExist ERROR : ', err)
      throw (err)
    }
    models.Members.where('FBID', inputJson.FBID).update({
      $set: {
        'pushTime': moment(inputJson.pushTime, 'HH:mm:ss')
      }
    }, function (error, updatePushTimeResult) {
      // error will be an Error if one occurred during the query
      if (error) {
        console.log('error:', error)
        throw (error)
      }
      callback(null, updatePushTimeResult)
    })
  })
}

function insertProvideNews (newsInfo, callback) {
  newsInfo.sendTime = moment().add(8, 'hours')
  // console.log('newsInfo:')
  // console.log(JSON.stringify(newsInfo, null, 2))
  var new_news = new models.ProvideNews(newsInfo)
  new_news.save(function (error, insertProvideNewsResult) {
    if (error) {
      console.log('error:', error)
      throw (error)
    }
    callback(null, insertProvideNewsResult)
  })
}

module.exports = {
  queryUserExist: queryUserExist,
  insertUser: insertUser,
  querySubscribeTopic: querySubscribeTopic,
  updateSubscribeTopic: updateSubscribeTopic,
  queryUserTimezone: queryUserTimezone,
  updatePushTime: updatePushTime,
  insertProvideNews: insertProvideNews
}
