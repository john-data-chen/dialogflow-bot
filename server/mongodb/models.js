var config = require(__base + '/server/config.js')
const mongoose = require('mongoose')
const MONGODB_URI = (process.env.MONGODB_URI || config.db.MONGODB_URI)

const MembersSchema = mongoose.Schema({
  FBID: { type: String },
  firstName: { type: String },
  lastName: { type: String },
  email: { type: String },
  gender: { type: String },
  joinTime: { type: Date },
  subscribeTopic: {
    latest: { type: Boolean, default: false },
    onepoint: { type: Boolean, default: false },
    recommend: { type: Boolean, default: false },
    hilight: { type: Boolean, default: false },
    entertainment: { type: Boolean, default: false },
    column: { type: Boolean, default: false },
    classic: { type: Boolean, default: false }
  },
  profile_pic: { type: String },
  locale: { type: String },
  timezone: { type: Number }
})
const Members = mongoose.model('Members', MembersSchema)
module.exports = Members

const BroadcastsSchema = mongoose.Schema({
  botId: { type: String },
  buttonTitle: { type: String },
  buttonURL: { type: String },
  broadcastId: { type: String },
  labelId: { type: String },
  label: { type: String },
  sendType: { type: String },
  msgType: { type: String },
  apiURL: { type: String },
  title: { type: String },
  subtitle: { type: String },
  imageURL: { type: String },
  creator: { type: String },
  pushTime: { type: Date },
  createTime: { type: Date },
  updateTime: {type: Date},
  pushed: { type: Boolean, default: false}
})
const Broadcasts = mongoose.model('Broadcasts', BroadcastsSchema, 'broadcasts')
module.exports = Broadcasts

const ProvideNewsSchema = mongoose.Schema({
  userId: { type: String },
  msgId: { type: String },
  content: { type: String },
  attachment: { type: String },
  sendTime: { type: Date }
})
const ProvideNews = mongoose.model('ProvideNews', ProvideNewsSchema, 'provideNews')
module.exports = ProvideNews

module.exports = {
  Members: Members,
  Broadcasts: Broadcasts,
  ProvideNews: ProvideNews
}
