// Limit of Ask Betty.xlsx
const FB_MAX_QUICK_REPLY = 11
// FB generic template, the official max of return == 10, so 10 - 1 = 9
const TEMP_LAST_ITEM_NUMBER = 9
// error handle when API return news without picture
const DEFAULT_TEMP_IMAGE_URL =
  'https://img.appledaily.com.tw/images/fb_sharelogo_1.jpg'
const FB_WELCOME_SPEECH =
  '您好，我是壹狗仔，您可以使用快速選單找尋您想看的新聞，也可以直接問我。登記註冊壹週刊會員「壹訂友」，獨家踢爆一定有！揭弊偷拍一定有！優惠好康一定有！\n\n'
const HOW_TO_USE_SPEECH = '例句:\n幫我找娛樂新聞\n幫我找蔡英文的新聞'
// quick replies (need title)
const CHOOSE_CATEGORY_SPEECH = '請選擇分類\n'
// return when no result
const NO_RESULT_SPEECH =
  '抱歉！你搜尋的條件，目前沒有符合的新聞，請更換條件，謝謝！'
// 爆料
// subscribe
const RETURN_SUBSCRIBED_SPEECH = '以下是您的訂閱情形：\n'
const HANGOVER_APP_ID = '263902037430900'
const NO_WEATHER_SPEECH =
  '您查詢的區域沒有有關天氣的資訊，要不要換個地方？(試試看 台北市的天氣)'
const WEATHER_API_URL =
  'http://mldev.api.twnextdigital.com/mob/v2/1/weather/city/'
const WEATHER_DEFAULT_AREA = {
  臺北市: '中正區',
  新北市: '板橋區',
  桃園市: '桃園區',
  新竹市: '北區',
  新竹縣: '竹北市',
  苗栗縣: '苗栗市',
  臺中市: '中區',
  彰化縣: '彰化市',
  南投縣: '南投市',
  雲林縣: '斗六市',
  嘉義市: '東區',
  嘉義縣: '梅山鄉',
  臺南市: '中西區',
  高雄市: '新興區',
  屏東縣: '屏東市',
  宜蘭縣: '宜蘭市',
  花蓮縣: '花蓮市',
  臺東縣: '臺東市',
  連江縣: '北竿鄉',
  金門縣: '金沙鎮',
  澎湖縣: '馬公市'
}
// for GA
const FB_GA_STRING = '?utm_source=facebook&utm_medium=chatbot'
// Bot version
const BOT_VERSION = '2.0'
// quick replies (not need title, put behind template)
const FB_QUICK_REPLY_OPTIONS = [
  {
    content_type: 'text',
    title: '最新',
    payload: '最新'
  },
  {
    content_type: 'text',
    title: '壹點就報',
    payload: '壹點就報'
  },
  {
    content_type: 'text',
    title: '推薦',
    payload: '推薦'
  },
  {
    content_type: 'text',
    title: '要聞',
    payload: '要聞'
  },
  {
    content_type: 'text',
    title: '娛樂',
    payload: '娛樂'
  },
  {
    content_type: 'text',
    title: '專欄',
    payload: '專欄'
  },
  {
    content_type: 'text',
    title: '經典檔案',
    payload: '經典檔案'
  },
  {
    content_type: 'text',
    title: '其他分類',
    payload: '其他分類'
  },
  {
    content_type: 'text',
    title: '爆料',
    payload: '爆料'
  },
  {
    content_type: 'text',
    title: '訂閱管理',
    payload: '訂閱管理'
  },
  {
    content_type: 'text',
    title: '說明',
    payload: '說明'
  }
]

const LIST_CATEGORY_OPTIONS = [
  {
    content_type: 'text',
    title: '熱門',
    payload: '熱門'
  },
  {
    content_type: 'text',
    title: '社會',
    payload: '社會'
  },
  {
    content_type: 'text',
    title: '人物',
    payload: '人物'
  },
  {
    content_type: 'text',
    title: '美妝保養',
    payload: '美妝保養'
  },
  {
    content_type: 'text',
    title: '生活',
    payload: '生活'
  },
  {
    content_type: 'text',
    title: '科技',
    payload: '科技'
  },
  {
    content_type: 'text',
    title: '時尚',
    payload: '時尚'
  },
  {
    content_type: 'text',
    title: '國際',
    payload: '國際'
  },
  {
    content_type: 'text',
    title: '美食旅遊',
    payload: '美食旅遊'
  }
]

const FB_SUBSCRIBE_TOPIC_OPTIONS = [
  {
    content_type: 'text',
    title: '查詢訂閱',
    payload: '查詢訂閱'
  },
  {
    content_type: 'text',
    title: '回到主選單',
    payload: '回到主選單'
  }
]

const FB_MODIFY_SUBSCRIBE_OPTIONS = [
  {
    content_type: 'text',
    title: '修改訂閱',
    payload: '修改訂閱'
  },
  {
    content_type: 'text',
    title: '回到主選單',
    payload: '回到主選單'
  }
]

const ASK_ATTACHMENT_OPTIONS = [
  {
    content_type: 'text',
    title: '有',
    payload: '有'
  },
  {
    content_type: 'text',
    title: '無',
    payload: '無'
  }
]

const DEFAULT_FALLBACK_SPEECH_POOL = [
  '對不起，我聽不懂你的問題。請參考例句：幫我找娛樂新聞',
  '這個格式才正確喔！例句：幫我找蔡英文的新聞'
]

const REPLY_STICKER_SPEECH_POOL = [
  '對不起，我看不懂貼圖。請您使用選單或是輸入 最新 來看看目前最新的新聞吧！',
  '對不起，我看不懂貼圖。請您使用選單或是輸入 熱門 來看看網友最愛看的新聞吧！',
  '對不起，我看不懂貼圖。請您使用選單或是輸入 其他分類 來看看您有興趣的新聞吧！',
  '對不起，我看不懂貼圖。請您使用選單或是輸入 搜尋 來找您想搜尋的新聞吧！'
]

var config = require(__base + '/server/config.js')
const SUBSCRIBE_TOPIC = [
  config.mother_lode.SERVER_URL + config.mother_lode.RTN_LATEST_NEWS_URL,
  // CAT news start from here
  config.mother_lode.SERVER_URL +
    config.mother_lode.RTN_CAT_NEWS_URL +
    config.CAT_ID.ONEPOINT,
  config.mother_lode.SERVER_URL +
    config.mother_lode.RTN_CAT_NEWS_URL +
    config.CAT_ID.RECOMMEND,
  config.mother_lode.SERVER_URL +
    config.mother_lode.RTN_CAT_NEWS_URL +
    config.CAT_ID.HILIGHT,
  config.mother_lode.SERVER_URL +
    config.mother_lode.RTN_CAT_NEWS_URL +
    config.CAT_ID.ENTERTAINMENT,
  config.mother_lode.SERVER_URL +
    config.mother_lode.RTN_CAT_NEWS_URL +
    config.CAT_ID.COLUMN,
  config.mother_lode.SERVER_URL +
    config.mother_lode.RTN_CAT_NEWS_URL +
    config.CAT_ID.CLASSIC
]
// template title
const TOPIC_TITLE_LIST = [
  '最新',
  '壹點就報',
  '推薦',
  '要聞',
  '娛樂',
  '專欄',
  '經典檔案'
]

// Targeting Broadcast Messages
const BROADCAST_LABEL_NAME = {
  latest: '最新',
  onepoint: '壹點就報',
  recommend: '推薦',
  hilight: '要聞',
  entertainment: '娛樂',
  column: '專欄',
  classic: '經典檔案'
}

module.exports = {
  FB_MAX_QUICK_REPLY: FB_MAX_QUICK_REPLY,
  TEMP_LAST_ITEM_NUMBER: TEMP_LAST_ITEM_NUMBER,
  FB_WELCOME_SPEECH: FB_WELCOME_SPEECH,
  HOW_TO_USE_SPEECH: HOW_TO_USE_SPEECH,
  CHOOSE_CATEGORY_SPEECH: CHOOSE_CATEGORY_SPEECH,
  NO_RESULT_SPEECH: NO_RESULT_SPEECH,
  FB_GA_STRING: FB_GA_STRING,
  BOT_VERSION: BOT_VERSION,
  FB_SUBSCRIBE_TOPIC_OPTIONS: FB_SUBSCRIBE_TOPIC_OPTIONS,
  FB_MODIFY_SUBSCRIBE_OPTIONS: FB_MODIFY_SUBSCRIBE_OPTIONS,
  RETURN_SUBSCRIBED_SPEECH: RETURN_SUBSCRIBED_SPEECH,
  HANGOVER_APP_ID: HANGOVER_APP_ID,
  NO_WEATHER_SPEECH: NO_WEATHER_SPEECH,
  WEATHER_API_URL: WEATHER_API_URL,
  WEATHER_DEFAULT_AREA: WEATHER_DEFAULT_AREA,
  FB_QUICK_REPLY_OPTIONS: FB_QUICK_REPLY_OPTIONS,
  LIST_CATEGORY_OPTIONS: LIST_CATEGORY_OPTIONS,
  SUBSCRIBE_TOPIC: SUBSCRIBE_TOPIC,
  TOPIC_TITLE_LIST: TOPIC_TITLE_LIST,
  BROADCAST_LABEL_NAME: BROADCAST_LABEL_NAME,
  ASK_ATTACHMENT_OPTIONS: ASK_ATTACHMENT_OPTIONS,
  DEFAULT_FALLBACK_SPEECH_POOL: DEFAULT_FALLBACK_SPEECH_POOL,
  REPLY_STICKER_SPEECH_POOL: REPLY_STICKER_SPEECH_POOL,
  DEFAULT_TEMP_IMAGE_URL: DEFAULT_TEMP_IMAGE_URL
}
