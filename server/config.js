var env = process.env.NODE_ENV

module.exports = (function () {
  if (env == 'development' || env == undefined || env == '') {
    return {
      dialogflow: {
        GOOGLE_CLIENT_EMAIL:
          'dialogflow@next-bot-dev-66508.iam.gserviceaccount.com',
        GOOGLE_PRIVATE_KEY:
          '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDmQqqTlJcyuEj0\nJra8xEivBQ18TA38tpYoMUzsw7an//NjBqi6cX+5u2xbMdjeFqdbfn52pdYSDkf4\nOfRoFN90SRUBos9YF0Ofe2VwUXYtNMP5AmWpYEBJPjTsyhSyjbBsl4hynVG2jugY\n6hS3XSm1sKmapybVBN3R5YBkuZeYkG+DDB9erz2jYvuikVrNPK0/JqA3BiAvEwMB\nVEFpi7vvZYox/vl6GSM5HyAH+slgZoLtdWOmffjHpCgdAdlx32z/PWFB542cytdV\nHroRriSxXZh/PZa8QBKy4iREheQSZ/iZmkMnJnuawsIUcZHEXbkvGS9lDX/pW+PU\nOXWYtUQNAgMBAAECggEAD8Vdqw2InJn4LzbOmdfE1a3+KjcIVziHSNn1l5rhEhkr\nCauNV3XKJoXCim172dxJ2q+riENcCfEN91+60H0rf+q4hFvrITC4uj3oqPx/YGHG\nkXHbVjJIPOwEknpV2BPx00I+v4DsdZlMptYzlIqOPopK15OeyoTSj5uNQ3jdu/55\nTgCMZWE97i/Gtjnz1UT4IeLjLmfnaXgksqhpbvYwS1lcd7rM2W/kYQmJMTdty/s1\nHI4n5cNFAl0oMZUDl/fWQxYMAIb76l0i2Y3mW8SS2bk3h95r5CzjewL21ABuT+Z3\nmZGz/BkrZU0u2cdeXgIIYRJFE465eNkGSVrqGOijWQKBgQD1nUh99H707ElZf0HV\njVeN0gpcbcqnINV2/97UurfrXZKYeKuVom9NZRBYyc/QD33l3To1qyIyYcELR49b\nZm5jpBhMAjDNttCVTA70dz8siij9sL7q4H40lp3sekVVYlPzN/ObsqQFy77ojnMT\nxgkTIWifDkRv4oiJR38V0dVxCQKBgQDv/y4ZjkDOM5LjVslSc05PA1DfWebre3PY\nd7xWhOb9wCreYpRxs6mstnLw49K3iQ5OWFFlZIAwNleKAcNaW7eg3VDZgRKdxhDF\n8DXNdsrtA9s8dRnINjjE83MJIEFPNQf5HhvD4eMdVxoVUrXXCnhFb6zvE/+nf4TF\nXL0kkvKv5QKBgQDPbNQS40/wojhT59/+bM6bS95KLsKtoFIGnJJgT4wRrsWmFIq7\n5SFgWFCU22KwkNS/MrT9LBjGOb4hO377LqddaPDICDWVFaZT35blqGtmvwtdLE4f\n6BVNbkHSu3QgZtkFF6SZQiW5a1c/6UWQVnfFezpy0UbEwqiSXxoZoEtEmQKBgAHq\nTGjhzLyYhsSPrb2ds/NHeH6/zer1KkCA/BzIpLtDrFPn/31EzMbXEeXAO69BRb70\nT3n6qlc4J8yvJOr0rmt3OKzzrb1alkmuGA/Ssd3gWQCLgrApVpyjwLhC7KKALtcb\nGWefxts2Fbwz08WqjLkpIf0qcLDagKm+irffAFbNAoGBAOSYo35EC+00u1ay0h9q\n8DLf3LTeflZqgcig2jeK20QbKxcECjM6xm3vlJfPRvNF45lIwJTJ5s28PVogkmhm\n+S9ru+gdNirFEs1Et58jfIw2KoKIav3AHfmGWH/5dWhcROXiwzKie/surcAepNnb\nnvVswYbMkMfDNxJgQy2996mq\n-----END PRIVATE KEY-----\n',
        GOOGLE_PROJECT_ID: 'next-bot-dev-66508',
        LANGUAGE_CODE: 'zh-CN'
      },
      facebook: {
        VERIFY_TOKEN: 'news',
        PAGE_ACCESS_TOKEN:
          'EAATBieP5BrUBAJ3keh6asqdLGASSCcsJOxwS7dEFHnuBvxfqIaK3wQygE2u1RvWcPF6XcDIO1K7jUMZBhuFuZCHgrtRGrVOwmzHWPlW1y9LfZCQ7Eae2PZBlizydDCIT9XJSbXBCNSXCiQYs82DOpknhds1Ie1k0XEq1uql3fAZDZD',
        APP_ID: '1338697886271157',
        APP_SECRET: '4e7fa61dff032ffc1d547374ac7a0d4f'
      },
      db: {
        // Dev
        MONGODB_URI: 'mongodb://172.23.9.103:27017/next_bot'
        // when setup in local test or admin Env
        // MONGODB_URI: 'mongodb://localhost:27017/next_bot'
      },
      port: 5003,
      mother_lode: {
        SERVER_URL: 'http://mluat.api.twnextdigital.com',
        // search keyword (tag)
        SEARCH_NEWS_URL: '/v1/2/Search?KeyWord=',
        // RTN
        RTN_HOT_NEWS_URL: '/v1/2/ArticleList?Type=RTN_ALL&sortBy=VIEW',
        RTN_LATEST_NEWS_URL: '/v1/2/ArticleList?Type=RTN_ALL&sortBy=LATEST',
        RTN_CAT_NEWS_URL: '/v1/2/ArticleList?Type=RTN&CatId='
      },
      CAT_ID: {
        SOCIETY: '400001',
        ENTERTAINMENT: '400003',
        PEOPLE: '400004',
        FASHION: '400005',
        LIFE: '400006',
        FOODANDTRAVEL: '400007',
        COLUMN: '400008',
        HILIGHT: '400011',
        TECH: '400012',
        INTERNATIONAL: '400013',
        RECOMMEND: '400020',
        ONEPOINT: '400014',
        CLASSIC: '400015',
        MAKEUP: '400016'
      },
      broadcastLabelId: {
        latest: '1183231578468147',
        hot: '2272337289456618',
        onepoint: '2646566228692844',
        recommend: '2430744563613103',
        hilight: '2213208328718542',
        society: '1971949006265688',
        entertainment: '2185117484904995',
        column: '2285586764839394',
        classic: '1682675428502433'
      },
      console_log: 'on',
      email: {
        transporterConfig: {
          host: 'smtp.gmail.com',
          port: 587,
          secure: false, // true for 465, false for 587
          auth: {
            user: 'appledaily.chatbot@gmail.com', // generated ethereal user
            pass: 'bot@appledaily0nly' // generated ethereal password
          }
        },
        mailOptions: {
          from: '"Next Magazine Bot" <appledaily.bot@gmail.com>',
          to: 'john.chen@appledaily.com.tw',
          subject: '爆料來源 FB 機器人(測試環境)',
          text: 'default text',
          html: 'default html'
        }
      }
    }
  }
  if (env == 'production') {
    return {
      apiai: {
        // Prd
        APIAI_ACCESS_TOKEN: '8f5238765db54e67a162f3cc490d1083',
        APIAI_LANG: 'zh-cn'
      },
      facebook: {
        FB_VERIFY_TOKEN: 'news',
        // Prd
        FB_PAGE_ACCESS_TOKEN:
          'EAAflnmOZCUXQBAFrQZBJykrBaRZBmaHNsS5e2DVS8LSNuQKz2WSFkhOZAjvWBVMZAaNuCHpdj6WDzAZBHvd83u0w2PeSyAkBcib3otLfy3c850Ol1Q1aBiS3TBdZCU9qJD3tXlGuC6O6ZCiI2yhI1vxBZAsaiLgchUrCkXub3L37FIQZDZD'
      },
      db: {
        // Prd
        MONGODB_URI: 'mongodb://172.23.9.103:27017/next_bot_prd'
      },
      port: {
        REST_PORT: 5003
      },
      mother_lode: {
        SERVER_URL: 'http://mluat.api.twnextdigital.com',
        // search keyword (tag)
        SEARCH_NEWS_URL: '/v1/2/Search?KeyWord=',
        // RTN
        RTN_HOT_NEWS_URL: '/v1/2/ArticleList?Type=RTN_ALL&sortBy=VIEW',
        RTN_LATEST_NEWS_URL: '/v1/2/ArticleList?Type=RTN_ALL&sortBy=LATEST',
        RTN_CAT_NEWS_URL: '/v1/2/ArticleList?Type=RTN&CatId='
      },
      CAT_ID: {
        SOCIETY: '400001',
        ENTERTAINMENT: '400003',
        PEOPLE: '400004',
        FASHION: '400005',
        LIFE: '400006',
        FOODANDTRAVEL: '400007',
        COLUMN: '400008',
        HILIGHT: '400011',
        TECH: '400012',
        INTERNATIONAL: '400013',
        RECOMMEND: '400020',
        ONEPOINT: '400014',
        CLASSIC: '400015',
        MAKEUP: '400016'
      },
      slack: {
        SWITCH: 'off',
        CH_URL:
          'https://hooks.slack.com/services/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'
      },
      console_log: 'off',
      email: {
        transporterConfig: {
          host: 'smtp.gmail.com',
          port: 587,
          secure: false, // true for 465, false for 587
          auth: {
            user: 'appledaily.chatbot@gmail.com', // generated ethereal user
            pass: 'bot@appledaily0nly' // generated ethereal password
          }
        },
        mailOptions: {
          from: '"Next Magazine Bot" <appledaily.bot@gmail.com>',
          to: '119@nextmedia.com.tw',
          subject: '爆料來源 FB 機器人',
          text: 'default text',
          html: 'default html'
        }
      },
      broadcastLabelId: {
        latest: '2350621851692784',
        hot: '2136788686376578',
        onepoint: '2224647237584803',
        recommend: '2204771369602605',
        hilight: '1991385534306527',
        society: '2520450494633838',
        entertainment: '2108002215964016',
        column: '1989609684499696',
        classic: '1692554140847277'
      }
    }
  }
})()
