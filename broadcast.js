'use strict'
global.__base = require('path').resolve(__dirname)

var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var constants = require(__base + '/server/constants.js')
var formatter = require(__base + '/service/formatter.js')
var models = require(__base + '/server/mongodb/models.js')

var _l = require('lodash')
var async = require('async')
var moment = require('moment')
var request = require('request')
var Schedule = require('node-schedule')

function queryEnablePush (callback) {
  models.Broadcasts.find({
    'pushed': false
  }).sort({
    'pushTime': 'desc'
  }).exec(function (err, results) {
    if (err) {
      console.log('error:', err)
      throw (err)
    }
    var broadcasts = []
    _l.forEach(results, function (item) {
      // if timezone is universal, need to add offest
      // moment().utcOffset('+0800').format('YYYY-MM-DD HH:mm')
      if (moment().format('YYYY-MM-DD HH:mm') == moment.utc(item.pushTime).format('YYYY-MM-DD HH:mm')) {
        broadcasts.push(item)
      }
    })
    callback(null, broadcasts)
  })
}

function markPushed (inputJson, callback) {
  if (inputJson.broadcast._id != '' && inputJson.broadcastId != undefined) {
    models.Broadcasts.findOneAndUpdate({
      '_id': inputJson.broadcast._id
    }, {
      'broadcastId': inputJson.broadcastId,
      'pushed': true
    }, function (err, results) {
      if (err) {
        console.log('error:', err)
        throw (err)
      }
      var newBroadcast = inputJson.broadcast.toObject()
      newBroadcast._id = undefined
      newBroadcast.broadcastId = undefined
      newBroadcast.pushTime = moment(newBroadcast.pushTime).add(1, 'day')
      return callback(null, newBroadcast)
    })
  }
}

function createDailyBroadcast (broadcast, callback) {
  var new_broadcast = new models.Broadcasts(broadcast)
  new_broadcast.save(function (error, insertBroadcastResult) {
    if (error) {
      console.log('error:', error)
      throw (error)
    }
    callback(null, insertBroadcastResult)
  })
}

var checkSchedule = function () {
  queryEnablePush(function (err, broadcasts) {
    if (err) {
      // error handling code goes here
      console.log('queryMembersSubscribe ERROR : ', err)
      throw (err)
    }
    async.eachSeries(broadcasts, function (broadcast, next) {
      setTimeout(function () {
        var fbMsgArray = [{
          'attachment': constants.FB_TEMPLATE
        }]
        async.waterfall([
          function (callback) {
            if (broadcast.msgType == 'single') {
              fbMsgArray[0].attachment.payload.elements.push({
                'title': broadcast.title,
                'image_url': broadcast.imageURL,
                'subtitle': broadcast.subtitle,
                'default_action': {
                  'type': 'web_url',
                  'url': broadcast.buttonURL + constants.FB_GA_STRING,
                  'messenger_extensions': false,
                  'webview_height_ratio': 'tall'
                },
                'buttons': [{
                  'type': 'web_url',
                  'title': broadcast.buttonTitle,
                  'url': broadcast.buttonURL + constants.FB_GA_STRING
                },
                  // share button
                  {
                    'type': 'element_share'
                  }
                ]
              })

              callback(null, fbMsgArray)
            }
            if (broadcast.msgType == 'multi') {
              formatter.newsApiData({
                'source': 'facebook',
                'apiURL': broadcast.apiURL,
                'label': broadcast.label,
                'broadcast': true
              }, function (err, newsDataSet) {
                if (err) {
                  // error handling code goes here
                  console.log('mlData ERROR : ', err)
                  throw (err)
                }
                if (newsDataSet == 'no result') {
                  // fix issue of no quick reply when no result
                  console.log('Error! api ' + newsDataSet)
                }
                // with results
                else {
                  fbMsgArray[0].attachment.payload.elements = newsDataSet.elements
                  callback(null, fbMsgArray)
                }
              })
            }
          }
        ], function (err, fbMsgArray) {
          request({
            method: 'POST',
            headers: {
              'content-type': 'application/json'
            },
            uri: 'https://graph.facebook.com/v3.3/me/message_creatives?access_token=' + config.facebook.FB_PAGE_ACCESS_TOKEN,
            body: JSON.stringify({
              'messages': fbMsgArray
            })
          }, function (err, response, getIdBody) {
            if (err) {
              console.log('err: ', err)
            }
            if (response.statusCode >= 400) {
              console.log('fb broadcast response.statusCode: ', response.statusCode)
            }
            var creativeId = JSON.parse(getIdBody).message_creative_id
            // after broadcast is pushed, creativeId becomes invalid, each creativeId can be used only once
            request({
              method: 'POST',
              headers: {
                'content-type': 'application/json'
              },
              uri: 'https://graph.facebook.com/v3.3/me/broadcast_messages?access_token=' + config.facebook.FB_PAGE_ACCESS_TOKEN,
              body: JSON.stringify({
                'message_creative_id': creativeId,
                'custom_label_id': broadcast.labelId,
                'tag': 'NON_PROMOTIONAL_SUBSCRIPTION'
              })
            }, function (err, response, body) {
              if (err) {
                console.log('err: ', err)
              }
              if (response.statusCode >= 400) {
                console.log('fb broadcast response.statusCode: ', response.statusCode)
              }
              // after broadcast is pushed, creativeId becomes invalid, each creativeId can be used only once
              markPushed({
                'broadcast': broadcast,
                'broadcastId': JSON.parse(body).broadcast_id
              }, function (err, newBroadcast) {
                if (newBroadcast.sendType == 'daily') {
                  createDailyBroadcast(newBroadcast, function (err, insertBroadcastResult) {
                    if (err) {
                      // error handling code goes here
                      console.log('insertBroadcast ERROR : ', err)
                      throw (err)
                    }
                  })
                }
              })
            })
          })
        })
        next(); // don't forget to execute the callback!
      }, 1000)
    }, function () {})
  })
}

Schedule.scheduleJob('* * * * *', checkSchedule) // update every minutes
