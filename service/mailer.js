var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}

var nodemailer = require('nodemailer')
var userHandler = require(__base + '/server/mongodb/userHandler.js')

function sendMail(mailBody) {

  // save provideNews into DB
  var provideNewsJson = mailBody
  if ('userId' in mailBody) {
    userHandler.insertProvideNews(provideNewsJson, function (err, insertProvideNews) {
      if (err) {
        // error handling code goes here
        console.log('insertProvideNews ERROR : ', err)
        throw (err)
      }
    })

    // remove no-used key
    mailBody.content = undefined
    mailBody.userId = undefined
  }

  nodemailer.createTestAccount((err, account) => {

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport(config.email.transporterConfig)

    // setup email data with unicode symbols
    let mailOptions = config.email.mailOptions
    if ('text' in mailBody) {
      mailOptions.html = undefined
      mailOptions.text = mailBody.text
    }
    if ('html' in mailBody) {
      mailOptions.text = undefined
      mailOptions.html = mailBody.html
      mailOptions.attachments = mailBody.attachments
    }
    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error)
      }
      console.log('Message sent: %s', info.messageId)
    })
  })
}

module.exports = {
  sendMail: sendMail
}