var async = require('async')
var request = require('request')

var constants = require(__base + '/server/constants.js')

var userHandler = require(__base + '/server/mongodb/userHandler.js')

function newsApiData (inputSet, callback) {
  // console.log('inputSet:')
  // console.log(JSON.stringify(inputSet, null, 2))
  request.get(inputSet.apiURL, (error, response, newsBody) => {
    // error handle
    if (error) {
      console.log(error)
      return callback(null, 'no result')
    }
    if (response.statusCode == 204 || response.statusCode >= 400) {
      error = 'app.post/search response.statusCode: ' + response.statusCode
      console.log(error)
      console.log(JSON.stringify(response, null, 2))
      return callback(null, 'no result')
    }

    // transfer to JSON
    newsBody = JSON.parse(newsBody)
    // console.log('newsBody')
    // console.log(JSON.stringify(newsBody, null, 2))

    // if no result, return it
    if (newsBody.content.length <= 0) {
      return callback(null, 'no result')
    }

    newsContent(
      {
        source: inputSet.source,
        newsBody: newsBody,
        label: inputSet.label,
        broadcast: inputSet.broadcast
      },
      function (err, newsContentResult) {
        if (err) {
          // error handling code goes here
          console.log('newsContentResult ERROR : ', err)
          throw err
        }
        // console.log('newsContentResult:')
        // console.log(JSON.stringify(newsContentResult, null, 2))
        return callback(null, newsContentResult)
      }
    )
  })
}

function newsContent (inputSet, callback) {
  var source = inputSet.source
  var speech = ''
  var label = inputSet.label
  var imageUrl = constants.DEFAULT_TEMP_IMAGE_URL
  var finalContent = []
  var lastItemNumber = constants.TEMP_LAST_ITEM_NUMBER
  if (inputSet.newsBody.content.length <= constants.TEMP_LAST_ITEM_NUMBER + 1) {
    lastItemNumber = inputSet.newsBody.content.length - 1
  }

  for (var i = 0; i < inputSet.newsBody.content.length; i++) {
    async.waterfall(
      [
        function (callback) {
          if (inputSet.newsBody.content[i].sharing.image.length > 5) {
            if (
              inputSet.newsBody.content[i].sharing.image.indexOf('https') <= -1
            ) {
              imageUrl = inputSet.newsBody.content[i].sharing.image.replace(
                'http',
                'https'
              )
            } else imageUrl = inputSet.newsBody.content[i].sharing.image
          }
          callback(null, imageUrl)
        },
        function (imageUrl, callback) {
          switch (source) {
            case 'facebook':
              var buttonTitle = '看更多'
              var item = {
                title: inputSet.newsBody.content[i].title,
                image_url: imageUrl,
                subtitle: inputSet.newsBody.content[i].description,
                default_action: {
                  type: 'web_url',
                  url:
                    inputSet.newsBody.content[i].sharing.url +
                    constants.FB_GA_STRING,
                  messenger_extensions: false,
                  webview_height_ratio: 'tall'
                },
                buttons: [
                  {
                    title: buttonTitle,
                    type: 'web_url',
                    url:
                      inputSet.newsBody.content[i].sharing.url +
                      constants.FB_GA_STRING,
                    messenger_extensions: false,
                    webview_height_ratio: 'tall'
                  },
                  // share button
                  {
                    type: 'element_share'
                  }
                ]
              }
              callback(null, item)
              break

            case 'line':
              var item = {
                thumbnailImageUrl: imageUrl,
                imageBackgroundColor: '#FFFFFF',
                title: inputSet.newsBody.content[i].title,
                text: inputSet.newsBody.content[i].description.substring(0, 59),
                default_action: {
                  type: 'uri',
                  label: '看更多',
                  uri:
                    inputSet.newsBody.content[i].sharing.url +
                    constants.LINE_GA_STRING
                },
                actions: [
                  {
                    type: 'uri',
                    label: '看更多',
                    uri:
                      inputSet.newsBody.content[i].sharing.url +
                      constants.LINE_GA_STRING
                  }
                ]
              }
              callback(null, item)
              break

            default:
              var item = {
                title: inputSet.newsBody.content[i].title,
                description: inputSet.newsBody.content[i].description,
                image_url: imageUrl,
                url: inputSet.newsBody.content[i].sharing.url
              }
              callback(null, item)
              break
          }
        },
        function (item, callback) {
          if (
            inputSet.broadcast != true &&
            source == 'facebook' &&
            label != undefined
          ) {
            item.buttons.push({
              title: '訂閱',
              type: 'postback',
              payload: '訂閱' + constants.BROADCAST_LABEL_NAME[label]
            })
            callback(null, item)
          } else callback(null, item)
        },
        function (item, callback) {
          switch (source) {
            case 'agent':
              speech +=
                inputSet.newsBody.content[i].title +
                '\n' +
                inputSet.newsBody.content[i].description +
                '\n'
              callback(null, speech, item)
              break

            case 'slack':
              item = {
                title: inputSet.newsBody.content[i].title,
                title_link:
                  inputSet.newsBody.content[i].sharing.url +
                  constants.SLACK_GA_STRING,
                text: inputSet.newsBody.content[i].description,
                image_url: imageUrl
              }
              callback(null, speech, item)
              break

            default:
              callback(null, speech, item)
              break
          }
        }
      ],
      function (err, speech, item) {
        finalContent.push(item)

        if (i == lastItemNumber) {
          switch (source) {
            case 'facebook':
            case 'line':
            case 'slack':
            case 'webSpeech':
              return callback(null, {
                elements: finalContent
              })

            default:
              return callback(null, {
                speech: speech
              })
          }
        }
      }
    )
  }
}

function subscribeTopic (FBID, callback) {
  userHandler.querySubscribeTopic(FBID, function (err, subscribeTopic) {
    var subscribeTopicArray = [
      subscribeTopic.latest,
      subscribeTopic.onepoint,
      subscribeTopic.recommend,
      subscribeTopic.hilight,
      subscribeTopic.entertainment,
      subscribeTopic.column,
      subscribeTopic.classic
    ]
    async.map(
      constants.SUBSCRIBE_TOPIC,
      function (url, callback) {
        request.get(url, function (error, response, newsBody) {
          // error handle
          if (error) {
            console.log(error)
            console.log('url: ', url)
            return callback(null, 'no result')
          }
          if (response.statusCode == 204 || response.statusCode >= 400) {
            error =
              'app.post/search response.statusCode: ' + response.statusCode
            console.log(error)
            console.log(JSON.stringify(response, null, 2))
            return callback(null, 'no result')
          }

          // transfer to JSON
          newsBody = JSON.parse(newsBody)
          callback(error, newsBody)
        })
      },
      function (err, results) {
        var finalContent = []
        for (var i = 0; i < results.length; i++) {
          var statusInTitle = '無'
          var subscribeButton = {
            type: 'postback',
            title: '訂閱' + constants.TOPIC_TITLE_LIST[i],
            payload: '訂閱' + constants.TOPIC_TITLE_LIST[i]
          }
          if (subscribeTopicArray[i] == true) {
            statusInTitle = '有'
            subscribeButton.title = '退訂' + constants.TOPIC_TITLE_LIST[i]
            subscribeButton.payload = '退訂' + constants.TOPIC_TITLE_LIST[i]
          }
          async.waterfall(
            [
              function (callback) {
                var imageUrl = constants.DEFAULT_TEMP_IMAGE_URL
                if (results[i].content[0].sharing.image.length > 5) {
                  imageUrl = results[i].content[0].sharing.image
                  callback(null, imageUrl)
                } else {
                  callback(null, imageUrl)
                }
              },
              function (imageUrl, callback) {
                var item = {
                  title:
                    constants.TOPIC_TITLE_LIST[i] +
                    ' (您的訂閱狀態：' +
                    statusInTitle +
                    ')',
                  image_url: imageUrl,
                  subtitle: results[i].content[0].description,
                  default_action: {
                    type: 'web_url',
                    url:
                      results[i].content[0].sharing.url +
                      constants.FB_GA_STRING,
                    messenger_extensions: false,
                    webview_height_ratio: 'tall'
                  },
                  buttons: [
                    subscribeButton,
                    {
                      title: '看更多',
                      type: 'web_url',
                      url:
                        results[i].content[0].sharing.url +
                        constants.FB_GA_STRING,
                      messenger_extensions: false,
                      webview_height_ratio: 'tall'
                    },
                    // share button
                    {
                      type: 'element_share'
                    }
                  ]
                }
                callback(null, item)
              }
            ],
            function (err, item) {
              finalContent.push(item)
              if (i == constants.TOPIC_TITLE_LIST.length - 1) {
                return callback(null, finalContent)
              }
            }
          )
        }
      }
    )
  })
}

function subscribeStatus (FBID, callback) {
  userHandler.querySubscribeTopic(FBID, function (err, subscribeTopic) {
    var subscribeTopicArray = [
      subscribeTopic.latest,
      subscribeTopic.onepoint,
      subscribeTopic.recommend,
      subscribeTopic.hilight,
      subscribeTopic.entertainment,
      subscribeTopic.column,
      subscribeTopic.classic
    ]
    var subscribeSummary = ''
    for (var i = 0; i < subscribeTopicArray.length; i++) {
      var status = '無'
      if (subscribeTopicArray[i] == true) {
        status = '有'
      }
      subscribeSummary += constants.TOPIC_TITLE_LIST[i] + ' : ' + status + '\n'
      if (i == subscribeTopicArray.length - 1) {
        return callback(null, subscribeSummary)
      }
    }
  })
}

module.exports = {
  newsApiData: newsApiData,
  subscribeTopic: subscribeTopic,
  subscribeStatus: subscribeStatus
}
