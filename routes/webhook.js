var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var constants = require(__base + '/server/constants.js')
// add log time
var userHandler = require(__base + '/server/mongodb/userHandler.js')

require('console-stamp')(console, 'yyyy.mm.dd HH:MM:ss.l')
const express = require('express')
const router = express.Router()

const sessionIds = new Map()
const fbService = require(__base + '/service/fb-service.js')
const dialogflowService = require(__base + '/service/dialogflow-service.js')
const TinyURL = require('tinyurl')

// import search
var advanceSearch = require(__base + '/routes/search/advanceSearch.js')
var defaultFallback = require(__base + '/routes/search/defaultFallback.js')
var getVersion = require(__base + '/routes/search/getVersion.js')
var hangover = require(__base + '/routes/search/hangover.js')
var modifySubscribeTopic = require(__base +
  '/routes/search/modifySubscribeTopic.js')
var noAttachment = require(__base + '/routes/search/noAttachment.js')
var returnNewsCategory = require(__base +
  '/routes/search/returnNewsCategory.js')
var returnSubscribeTopic = require(__base +
  '/routes/search/returnSubscribeTopic.js')
var searchRTN = require(__base + '/routes/search/searchRTN.js')
var sendAttachment = require(__base + '/routes/search/sendAttachment.js')
var startSearch = require(__base + '/routes/search/startSearch.js')
var stickerReply = require(__base + '/routes/search/stickerReply.js')
var subscribe = require(__base + '/routes/search/subscribe.js')
var weather = require(__base + '/routes/search/weather.js')
var welcome = require(__base + '/routes/search/welcome.js')

router.get('/', function (req, res) {
  if (
    req.query['hub.mode'] === 'subscribe' &&
    req.query['hub.verify_token'] === config.facebook.VERIFY_TOKEN
  ) {
    res.status(200).send(req.query['hub.challenge'])
  } else {
    console.error('Failed validation. Make sure the validation tokens match.')
    res.sendStatus(403)
  }
})

router.post('/', function (req, res) {
  console.log(require('util').inspect(req.body, { depth: null }))
  if (req.body.object == 'page') {
    // Iterate over each entry
    // There may be multiple if batched
    req.body.entry.forEach(function (pageEntry) {
      // Secondary Receiver is in control - listen on standby channel
      if (pageEntry.standby) {
        // iterate webhook events from standby channel
        pageEntry.standby.forEach(event => {
          const psid = event.sender.id
          const message = event.message
          console.log('message from: ', psid)
          console.log('message to inbox: ', message)
        })
      }

      // Bot is in control - listen for messages
      if (pageEntry.messaging) {
        // Iterate over each messaging event
        pageEntry.messaging.forEach(function (messagingEvent) {
          if (messagingEvent.optin) {
            fbService.receivedAuthentication(messagingEvent)
          } else if (messagingEvent.message) {
            receivedMessage(messagingEvent)
          } else if (messagingEvent.postback) {
            receivedPostback(messagingEvent)
          } else if (messagingEvent.account_linking) {
            fbService.receivedAccountLink(messagingEvent)
          } else if (messagingEvent.pass_thread_control) {
            // do something with the metadata: messagingEvent.pass_thread_control.metadata
          } else {
            console.log(
              'Webhook received unknown messagingEvent: ',
              messagingEvent
            )
          }
        })
      }
    })

    // Assume all went well.
    // You must send back a 200, within 20 seconds
    res.sendStatus(200)
  }
  if (req.body.queryResult) {
    switch (req.body.queryResult.action) {
      case 'input.welcome':
      case 'returnHowToUse':
        welcome.searchResultJson(req.body)
        break

      case 'input.unknown':
        defaultFallback.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      case 'searchRTN':
        searchRTN.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      case 'startSearch':
        startSearch.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      case 'advanceSearch':
        advanceSearch.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      case 'sendAttachment':
        sendAttachment.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      case 'noAttachment':
        noAttachment.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      case 'subscribe':
        subscribe.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      case 'returnSubscribeTopic':
        returnSubscribeTopic.searchResultJson(req.body, function (
          err,
          resultJson
        ) {
          callback(null, resultJson)
        })
        break

      case 'modifySubscribeTopic':
        modifySubscribeTopic.searchResultJson(req.body, function (
          err,
          resultJson
        ) {
          callback(null, resultJson)
        })
        break

      case 'getVersion':
        getVersion.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      case 'chooseRTN':
        returnNewsCategory.searchResultJson(req.body, function (
          err,
          resultJson
        ) {
          callback(null, resultJson)
        })
        break

      case 'stickerReply':
        stickerReply.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      case 'hangover':
        hangover.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      case 'weather':
        weather.searchResultJson(req.body, function (err, resultJson) {
          callback(null, resultJson)
        })
        break

      default:
        console.log('search.js default')
        console.log('result.action: ', req.body.result.action)
    }
  }
})

function setSessionAndUser (senderID) {
  if (!sessionIds.has(senderID)) {
    sessionIds.set(senderID, 'fb:' + senderID)
  }
}

function receivedMessage (event) {
  var senderID = event.sender.id
  var message = event.message

  setSessionAndUser(senderID)

  // console.log("Received message for user %d and page %d at %d with message:", senderID, recipientID, timeOfMessage)
  console.log(JSON.stringify(message))

  var isEcho = message.is_echo
  var messageId = message.mid
  var appId = message.app_id
  var metadata = message.metadata

  // You may get a text or attachment but not both
  var messageText = message.text
  var quickReply = message.quick_reply

  if (isEcho) {
    fbService.handleEcho(messageId, appId, metadata)
    return
  } else if (quickReply) {
    handleQuickReply(senderID, quickReply)
    return
  }

  if (messageText) {
    // send message to DialogFlow
    dialogflowService.sendTextQueryToDialogFlow(
      sessionIds,
      handleDialogFlowResponse,
      senderID,
      messageText
    )
  }

  if (message.attachments) {
    TinyURL.shorten(message.attachments[0].payload.url).then(
      function (text) {
        if (message.sticker_id) text = 'sticker'
        dialogflowService.sendTextQueryToDialogFlow(
          sessionIds,
          handleDialogFlowResponse,
          senderID,
          text
        )
      },
      function (err) {
        console.log(err)
      }
    )
  }
}

function receivedPostback (event) {
  var senderID = event.sender.id
  var recipientID = event.recipient.id
  var timeOfPostback = event.timestamp

  setSessionAndUser(senderID)

  // The 'payload' param is a developer-defined field which is set in a postback
  // button for Structured Messages.
  var payload = event.postback.payload
  console.log(
    "Received postback for user %d and page %d with payload '%s' " + 'at %d',
    senderID,
    recipientID,
    payload,
    timeOfPostback
  )
  // send postback
  dialogflowService.sendTextQueryToDialogFlow(
    sessionIds,
    handleDialogFlowResponse,
    senderID,
    payload
  )
  // save user profile after press start button
  if (
    (payload =
      'FACEBOOK_WELCOME' &&
      senderID != undefined &&
      config.db.MONGODB_URI != '')
  ) {
    userHandler.queryUserExist(
      {
        FBID: senderID
      },
      function (err, queryUserExist) {
        if (err) {
          // error handling code goes here
          console.log('queryUserExist ERROR : ', err)
          throw err
        }
        // console.log(JSON.stringify(queryUserExist, null, 2))
      }
    )
  }
}

function handleQuickReply (senderID, quickReply) {
  dialogflowService.sendTextQueryToDialogFlow(
    sessionIds,
    handleDialogFlowResponse,
    senderID,
    quickReply.payload
  )
}

function handleDialogFlowResponse (sender, response) {
  let responseText = response.fulfillmentMessages.fulfillmentText

  let messages = response.fulfillmentMessages
  let action = response.action
  let contexts = response.outputContexts
  let parameters = response.parameters

  if (fbService.isDefined(action)) {
    handleDialogFlowAction(sender, action, messages, contexts, parameters)
  } else if (fbService.isDefined(messages)) {
    fbService.handleMessages(messages, sender)
  } else if (responseText == '' && !fbService.isDefined(action)) {
    // dialogflow could not evaluate input.
    fbService.sendTextMessage(sender, constants.FB_WELCOME_SPEECH)
  } else if (fbService.isDefined(responseText)) {
    fbService.sendTextMessage(sender, responseText)
  }
}

function handleDialogFlowAction (
  sender,
  action,
  messages,
  contexts,
  parameters
) {
  fbService.handleMessages(messages, sender)
}

module.exports = router
