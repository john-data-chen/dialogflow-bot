var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var mailer = reqlib('service/mailer')

function searchResultJson (body) {
  mailer.sendMail({
    content: body.queryResult.parameters.any,
    userId: body.session.split('fb:')[1],
    text: body.queryResult.parameters.any
  })
}

module.exports = {
  searchResultJson: searchResultJson
}
