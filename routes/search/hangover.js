var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var constants = reqlib('server/constants')

var request = require('request')

function searchResultJson (body) {
  request(
    {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      uri:
        'https://graph.facebook.com/v2.6/me/pass_thread_control?access_token=' +
        config.facebook.PAGE_ACCESS_TOKEN,
      body: JSON.stringify({
        recipient: { id: body.session.split('fb:')[1] },
        target_app_id: constants.HANGOVER_APP_ID
      })
    },
    function (err, response) {
      if (err) {
        console.log('err: ', err)
      }
      if (response.statusCode >= 400) {
        console.log('hangover response.statusCode: ', response.statusCode)
      }
    }
  )
}

module.exports = {
  searchResultJson: searchResultJson
}
