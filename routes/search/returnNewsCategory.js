var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var constants = reqlib('server/constants')
const fbService = require(__base + '/service/fb-service.js')

function searchResultJson (body) {
  var resultObj = {
    title: constants.CHOOSE_CATEGORY_SPEECH,
    quick_replies: constants.LIST_CATEGORY_OPTIONS
  }
  fbService.sendQuickReply(
    body.session.split('fb:')[1],
    resultObj.title,
    resultObj.quick_replies
  )
}

module.exports = {
  searchResultJson: searchResultJson
}
