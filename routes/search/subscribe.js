var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var constants = reqlib('server/constants')
var formatter = reqlib('service/formatter')
const fbService = require(__base + '/service/fb-service.js')

function searchResultJson (body) {
  var resultObj = {
    title: constants.RETURN_SUBSCRIBED_SPEECH,
    quick_replies: constants.FB_MODIFY_SUBSCRIBE_OPTIONS
  }

  formatter.subscribeStatus(body.session.split('fb:')[1], function (
    err,
    subscribeSummary
  ) {
    resultObj.title += subscribeSummary
    fbService.sendQuickReply(
      body.session.split('fb:')[1],
      resultObj.title,
      resultObj.quick_replies
    )
  })
}

module.exports = {
  searchResultJson: searchResultJson
}
