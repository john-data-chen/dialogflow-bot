var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var constants = reqlib('server/constants')
const fbService = require(__base + '/service/fb-service.js')

var async = require('async')
var userHandler = require(__base + '/server/mongodb/userHandler.js')

var request = require('request')

function searchResultJson (body) {
  const userId = body.session.split('fb:')[1]
  var resultObj = {
    title: '',
    quick_replies: constants.FB_SUBSCRIBE_TOPIC_OPTIONS
  }

  async.waterfall(
    [
      function (callback) {
        if (body.queryResult.queryText.indexOf('訂閱') > -1) {
          var subscribe = true
          var requestMethod = 'POST'
        }
        if (body.queryResult.queryText.indexOf('退訂') > -1) {
          var subscribe = false
          var requestMethod = 'DELETE'
        }
        callback(null, subscribe, requestMethod)
      },
      function (subscribe, requestMethod, callback) {
        userHandler.querySubscribeTopic(userId, function (err, subscribeTopic) {
          if (err) {
            console.log(err)
          }
          var labelId = ''
          var subscribeStatus = subscribeTopic
          console.log(config.broadcastLabelId.latest);
          
          switch (body.queryResult.parameters.category) {
            case '最新':
              subscribeStatus.latest = subscribe
              labelId = config.broadcastLabelId.latest
              break

            case '壹點就報':
                subscribeStatus.onepoint = subscribe
              labelId = config.broadcastLabelId.onepoint
              break

            case '推薦':
                subscribeStatus.recommend = subscribe
              labelId = config.broadcastLabelId.recommend
              break

            case '要聞':
                subscribeStatus.hilight = subscribe
              labelId = config.broadcastLabelId.hilight
              break

            case '娛樂':
                subscribeStatus.entertainment = subscribe
              labelId = config.broadcastLabelId.entertainment
              break

            case '專欄':
                subscribeStatus.column = subscribe
              labelId = config.broadcastLabelId.column
              break

            case '經典檔案':
                subscribeStatus.classic = subscribe
              labelId = config.broadcastLabelId.classic
              break

            default:
              console.log('topic default: error')
              break
          }
          callback(null, subscribeStatus, requestMethod, labelId)
        })
      }
    ],
    function (err, subscribeTopic, requestMethod, labelId) {
      request(
        {
          method: requestMethod,
          headers: {
            'content-type': 'application/json'
          },
          uri:
            'https://graph.facebook.com/v2.11/' +
            labelId +
            '/label?access_token=' +
            config.facebook.PAGE_ACCESS_TOKEN,
          body: JSON.stringify({
            user: userId
          })
        },
        function (err, response, body) {
          if (err) {
            console.log('err: ', err)
          }
          if (response.statusCode >= 400) {
            console.log(
              'fb broadcast response.statusCode: ',
              response.statusCode
            )
          }
        }
      )

      userHandler.updateSubscribeTopic(
        { FBID: userId, subscribeTopic: subscribeTopic },
        function (err, result) {
          if (err) {
            console.log('updateSubscribeTopic ERROR : ', err)
            throw err
          }
          resultObj.title = body.queryResult.queryText + '成功'
          fbService.sendQuickReply(
            // FB userID
            userId,
            resultObj.title,
            resultObj.quick_replies
          )
        }
      )
    }
  )
}

module.exports = {
  searchResultJson: searchResultJson
}
