var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var constants = reqlib('server/constants')
var formatter = reqlib('service/formatter')
const fbService = require(__base + '/service/fb-service.js')

function searchResultJson (body) {
  const source = 'facebook'
  var resultObj = {
    title: constants.NO_RESULT_SPEECH,
    quick_replies: constants.FB_QUICK_REPLY_OPTIONS
  }

  var apiURL =
    config.mother_lode.SERVER_URL +
    config.mother_lode.SEARCH_NEWS_URL +
    encodeURIComponent(body.queryResult.queryText)

  formatter.newsApiData(
    {
      source: source,
      apiURL: apiURL
    },
    function (err, newsDataSet) {
      if (err) {
        // error handling code goes here
        console.log('mlData ERROR : ', err)
        throw err
      }
      // when no result, return default
      if (newsDataSet == 'no result') {
        fbService.sendQuickReply(
          // FB userID
          body.session.split('fb:')[1],
          resultObj.title,
          resultObj.quick_replies
        )
      }
      // with results
      else {
        fbService.sendGenericMessage(
          // FB userID
          body.session.split('fb:')[1],
          newsDataSet.elements
        )
      }
    }
  )
}

module.exports = {
  searchResultJson: searchResultJson
}
