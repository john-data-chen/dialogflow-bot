var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var constants = reqlib('server/constants')
const fbService = require(__base + '/service/fb-service.js')

function searchResultJson (body) {
  var resultObj = {
    title: constants.FB_WELCOME_SPEECH,
    quick_replies: constants.FB_QUICK_REPLY_OPTIONS
  }
  if (body.queryResult.action == 'returnHowToUse') {
    resultObj.title += constants.HOW_TO_USE_SPEECH
  }
  fbService.sendQuickReply(
    body.session.split('fb:')[1],
    resultObj.title,
    resultObj.quick_replies
  )
}

module.exports = {
  searchResultJson: searchResultJson
}
