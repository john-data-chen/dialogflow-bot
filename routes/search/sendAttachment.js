var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}

var mailer = reqlib('service/mailer')

function searchResultJson(body) {
  var htmlBody =
    body.queryResult.parameters.getNewsAny
      .replace('\n', '<p>')
      .replace('\n', '<p>') +
    '<p>' +
    '<a href="' +
    body.queryResult.queryText +
    '">附檔連結</a>'
  mailer.sendMail({
    content: body.queryResult.parameters.getNewsAny,
    userId: body.session.split('fb:')[1],
    html: htmlBody
  })
}

module.exports = {
  searchResultJson: searchResultJson
}
