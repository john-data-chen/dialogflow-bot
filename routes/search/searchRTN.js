var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var constants = reqlib('server/constants')
var formatter = reqlib('service/formatter')
const fbService = require(__base + '/service/fb-service.js')

function searchResultJson(body) {
  const source = 'facebook'
  var resultObj = {
    title: constants.NO_RESULT_SPEECH,
    quick_replies: constants.FB_QUICK_REPLY_OPTIONS
  }

  var apiURL =
    config.mother_lode.SERVER_URL + config.mother_lode.RTN_HOT_NEWS_URL
  var label = ''

  switch (body.queryResult.queryText) {
    case '最新':
      apiURL =
        config.mother_lode.SERVER_URL + config.mother_lode.RTN_LATEST_NEWS_URL
      label = 'latest'
      break

    case '熱門':
      break

    case '壹點就報':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.ONEPOINT
      label = 'onepoint'
      break

    case '推薦':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.RECOMMEND
      label = 'recommend'
      break

    case '要聞':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.HILIGHT
      label = 'hilight'
      break

    case '社會':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.SOCIETY
      break

    case '娛樂':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.ENTERTAINMENT
      label = 'entertainment'
      break

    case '人物':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.PEOPLE
      break

    case '美妝':
    case '保養':
    case '美妝保養':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.MAKEUP
      break

    case '生活':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.LIFE
      break

    case '科技':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.TECH
      break

    case '時尚':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.FASHION
      break

    case '國際':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.INTERNATIONAL
      break

    case '美食':
    case '旅遊':
    case '美食旅遊':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.FOODANDTRAVEL
      break

    case '經典':
    case '經典檔案':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.CLASSIC
      label = 'classic'
      break

    case '專欄':
      apiURL =
        config.mother_lode.SERVER_URL +
        config.mother_lode.RTN_CAT_NEWS_URL +
        config.CAT_ID.COLUMN
      label = 'column'
      break

    default:
      console.log('default, set to hot')
      break
  }

  formatter.newsApiData(
    {
      source: source,
      apiURL: apiURL,
      label: label
    },
    function (err, newsDataSet) {
      if (err) {
        // error handling code goes here
        console.log('mlData ERROR : ', err)
        throw err
      }
      // when no result, return default
      if (newsDataSet == 'no result') {
        fbService.sendQuickReply(
          // FB userID
          body.session.split('fb:')[1],
          resultObj.title,
          resultObj.quick_replies
        )
      }
      // with results
      else {
        fbService.sendGenericMessage(
          // FB userID
          body.session.split('fb:')[1],
          newsDataSet.elements
        )
      }
    }
  )
}

module.exports = {
  searchResultJson: searchResultJson
}
