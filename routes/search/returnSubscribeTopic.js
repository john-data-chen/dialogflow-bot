var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var formatter = reqlib('service/formatter')
const fbService = require(__base + '/service/fb-service.js')

function searchResultJson (body) {
  formatter.subscribeTopic(body.session.split('fb:')[1], function (
    err,
    topicSet
  ) {
    fbService.sendGenericMessage(
      // FB userID
      body.session.split('fb:')[1],
      topicSet
    )
  })
}
module.exports = {
  searchResultJson: searchResultJson
}
