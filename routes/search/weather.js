var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
var constants = reqlib('server/constants')
const fbService = require(__base + '/service/fb-service.js')

var request = require('request')

function searchResultJson (body) {
  var resultObj = {
    title: constants.NO_WEATHER_SPEECH,
    quick_replies: constants.FB_QUICK_REPLY_OPTIONS
  }
  var city = '臺北市'
  var uviLevel = '沒有紫外線數據'
  var epaStatus = '沒有空氣品質數據'

  switch (body.queryResult.parameters['geo-city']) {
    case '基隆':
    case '基隆市':
      city = '基隆市'
      break

    case '台北':
    case '台北市':
    case '臺北':
    case '臺北市':
      city = '臺北市'
      break

    case '新北':
    case '新北市':
      city = '新北市'
      break

    case '桃園':
    case '桃園市':
      city = '桃園市'
      break

    case '新竹市':
      city = '新竹市'
      break

    case '新竹':
    case '新竹縣':
      city = '新竹縣'
      break

    case '苗栗':
    case '苗栗縣':
      city = '苗栗縣'
      break

    case '台中':
    case '台中市':
    case '臺中':
    case '臺中市':
      city = '臺中市'
      break

    case '彰化':
    case '彰化縣':
      city = '彰化縣'
      break

    case '南投':
    case '南投縣':
      city = '南投縣'
      break

    case '雲林':
    case '雲林縣':
      city = '雲林縣'
      break

    case '嘉義市':
      city = '嘉義市'
      break

    case '嘉義':
    case '嘉義縣':
      city = '嘉義縣'
      break

    case '台南':
    case '台南市':
    case '臺南':
    case '臺南市':
      city = '臺南市'
      break

    case '高雄':
    case '高雄市':
      city = '高雄市'
      break

    case '屏東':
    case '屏東縣':
      city = '屏東縣'
      break

    case '宜蘭':
    case '宜蘭縣':
      city = '宜蘭縣'
      break

    case '花蓮':
    case '花蓮縣':
      city = '花蓮縣'
      break

    case '台東':
    case '台東縣':
    case '臺東':
    case '臺東縣':
      city = '臺東縣'
      break

    case '連江':
    case '連江縣':
      city = '連江縣'
      break

    case '金門':
    case '金門縣':
      city = '金門縣'
      break

    case '澎湖':
    case '澎湖縣':
      city = '澎湖縣'
      break

    default:
      console.log('default')
      break
  }

  request.get(
    constants.WEATHER_API_URL +
      encodeURI(city) +
      '/county/' +
      encodeURI(constants.WEATHER_DEFAULT_AREA[city]),
    (error, response, weather) => {
      // error handle
      if (error) {
        console.log(error)
        fbService.sendQuickReply(
          body.session.split('fb:')[1],
          resultObj.title,
          resultObj.quick_replies
        )
      }
      if (response.statusCode == 204 || response.statusCode >= 400) {
        console.log(JSON.stringify(response, null, 2))
        fbService.sendQuickReply(
          body.session.split('fb:')[1],
          resultObj.title,
          resultObj.quick_replies
        )
      }
      weather = JSON.parse(weather)
      if (weather.content.currentWeather.UVI_level != undefined) { uviLevel = weather.content.currentWeather.UVI_level }
      if (weather.content.currentEPA.Status != undefined) { epaStatus = weather.content.currentEPA.Status }
      resultObj.title =
        city +
        '的天氣：' +
        weather.content.currentWeather.desc +
        '\n紫外線量級：' +
        uviLevel +
        '\n空氣品質：' +
        epaStatus
      fbService.sendQuickReply(
        body.session.split('fb:')[1],
        resultObj.title,
        resultObj.quick_replies
      )
    }
  )
}

module.exports = {
  searchResultJson: searchResultJson
}
