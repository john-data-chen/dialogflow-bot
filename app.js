'use strict'
global.reqlib = require('app-root-path').require
global.__base = require('path').resolve(__dirname)
var config = require(__base + '/server/config.js')
if (config.console_log == 'off') {
  console.log = function () {}
}
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
app.set('port', config.port || 5003)

const passport = require('passport')
const session = require('express-session')

const webhook = require('./routes/webhook')

app.use(bodyParser.json({
  verify: function (req, res, buf, encoding) {
    // raw body for line signature check
    req.rawBody = buf.toString()
  }
}))

// serve static files in the public directory
app.use(express.static('public'))

// Process application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    extended: false
  })
)

// Process application/json
app.use(bodyParser.json())

app.use(
  session({
    secret: config.facebook.VERIFY_TOKEN,
    resave: true,
    saveUninitialized: true
  })
)

app.use(passport.initialize())
app.use(passport.session())

passport.serializeUser(function (profile, cb) {
  cb(null, profile)
})

passport.deserializeUser(function (profile, cb) {
  cb(null, profile)
})

app.get(
  '/auth/facebook',
  passport.authenticate('facebook', { scope: 'public_profile' })
)

app.get(
  '/auth/facebook/callback',
  passport.authenticate('facebook', {
    successRedirect: '/broadcast/broadcast',
    failureRedirect: '/broadcast'
  })
)

app.set('view engine', 'ejs')

// Index route
app.get('/', function (req, res) {
  res.send('Hello world, I am a chat bot')
})

app.use('/webhook', webhook)

app.listen(app.get('port'), function () {
  console.log('running on port', app.get('port'))
})
